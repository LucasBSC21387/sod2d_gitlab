module mod_bc_routines

   use mod_mpi
   use mod_numerical_params
   use mod_comms
   use mod_comms_boundaries
   use mod_nvtx

   implicit none

      real(rp), allocatable, dimension(:)   :: aux_rho2,aux_p2,aux_E2
      real(rp), allocatable, dimension(:,:) :: aux_q2,aux_u2
      logical  :: allocate_memory_bcc = .true.

      contains

         subroutine temporary_bc_routine_dirichlet_prim(npoin,nboun,bou_codes,bou_codes_nodes,bound,nbnodes,lbnodes,lnbn_nodes,normalsAtNodes,aux_rho,aux_q,aux_u,aux_p,aux_E,u_buffer,u_mapped)

            implicit none

            integer(4), intent(in)     :: npoin, nboun, bou_codes(nboun), bou_codes_nodes(npoin), bound(nboun,npbou)
            integer(4), intent(in)     :: nbnodes, lbnodes(nbnodes),lnbn_nodes(npoin)
            real(rp), intent(in)     :: normalsAtNodes(npoin,ndime),u_buffer(npoin,ndime),u_mapped(npoin,ndime)
            real(rp),    intent(inout) :: aux_rho(npoin),aux_q(npoin,ndime),aux_u(npoin,ndime),aux_p(npoin),aux_E(npoin)
            integer(4)                 :: iboun,bcode,ipbou,inode,idime,iBoundNode
            real(rp)                   :: cin,R_plus,R_minus,v_b,c_b,s_b,rho_b,p_b,rl,rr, sl, sr
            real(rp)                   :: q_hll,rho_hll,E_hll,E_inf,norm,y,T_inf,p_inf,nscbc_g,aux_u_mag,aux_u_2_mag,temp

            nscbc_g = sqrt(nscbc_g_x**2+nscbc_g_y**2+nscbc_g_z**2)

            if(allocate_memory_bcc) then
               allocate_memory_bcc = .false.

               allocate(aux_rho2(npoin),aux_p2(npoin),aux_E2(npoin))
               !$acc enter data create(aux_rho2(:))
               !$acc enter data create(aux_p2(:))
               !$acc enter data create(aux_E2(:))
               allocate(aux_u2(npoin,ndime),aux_q2(npoin,ndime))
               !$acc enter data create(aux_u2(:,:))
               !$acc enter data create(aux_q2(:,:))
            end if

            !$acc parallel loop
            do inode = 1,npoin
               if(bou_codes_nodes(inode) .lt. max_num_bou_codes) then
                  aux_q2(inode,1) = aux_q(lnbn_nodes(inode),1)
                  aux_q2(inode,2) = aux_q(lnbn_nodes(inode),2)
                  aux_q2(inode,3) = aux_q(lnbn_nodes(inode),3)
                  aux_rho2(inode) = aux_rho(lnbn_nodes(inode))
                  aux_E2(inode) = aux_E(lnbn_nodes(inode))
               end if
            end do
            !$acc end parallel loop

            if(mpi_size.ge.2) then
               call nvtxStartRange("MPI_comms_tI")
               call mpi_halo_bnd_atomic_max_real_massEnerMom_iSendiRcv(aux_rho2(:),aux_E2(:),aux_q2(:,:))
               call nvtxEndRange
            end if

            !$acc parallel loop
            do inode = 1,npoin
               if(bou_codes_nodes(inode) .lt. max_num_bou_codes) then
                  aux_u2(inode,1) = aux_q2(inode,1)/aux_rho2(inode)
                  aux_u2(inode,2) = aux_q2(inode,2)/aux_rho2(inode)
                  aux_u2(inode,3) = aux_q2(inode,3)/aux_rho2(inode)

                  aux_p2(inode) = aux_rho2(inode)*(nscbc_gamma_inf-1.0_rp)*((aux_E2(inode)/aux_rho2(inode))- &
                     0.5_rp*((aux_u2(inode,1)*aux_u2(inode,1)) + (aux_u2(inode,2)*aux_u2(inode,2)) +(aux_u2(inode,3)*aux_u2(inode,3))))
               end if
            end do
            !$acc end parallel loop

            !$acc parallel loop
            do inode = 1,npoin
               if(bou_codes_nodes(inode) .lt. max_num_bou_codes) then
                  bcode = bou_codes_nodes(inode) ! Boundary element code
                  if (bcode == bc_type_far_field) then ! inlet just for aligened inlets with x

                     aux_u_2_mag = dot_product(aux_u2(inode,:),aux_u2(inode,:))
                     E_inf = (nscbc_rho_inf*0.5_rp*nscbc_u_inf**2 + nscbc_p_inf/(nscbc_gamma_inf-1.0_rp))

                     sl = min(nscbc_u_inf-nscbc_c_inf, aux_u_2_mag - sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)))
                     sr =  max(aux_u_2_mag + sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)), nscbc_u_inf+nscbc_c_inf)

                     rho_hll = (sr*aux_rho2(inode)-sl*nscbc_rho_inf+nscbc_rho_inf*nscbc_u_inf-aux_rho(inode)*aux_u_2_mag)/(sr-sl)
                     E_hll   = (sr*aux_E2(inode)-sl*E_inf+nscbc_u_inf*(E_inf+nscbc_p_inf)-aux_u_2_mag*(aux_E2(inode)+aux_p2(inode)))/(sr-sl)

                     sl = min(u_buffer(inode,1)-nscbc_c_inf, aux_u2(inode,1) - sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)))
                     sr =  max(aux_u2(inode,1) + sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)), u_buffer(inode,1)+nscbc_c_inf)

                     q_hll   = (sr*aux_q2(inode,1)-sl*nscbc_rho_inf*u_buffer(inode,1)+nscbc_rho_inf*u_buffer(inode,1)**2-aux_u2(inode,1)*aux_q2(inode,1))/(sr-sl)

                     aux_rho(inode) = rho_hll
                     aux_E(inode) = E_hll
                     aux_q(inode,1) = q_hll

                     sl = min(u_buffer(inode,2)-nscbc_c_inf, aux_u2(inode,2) - sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)))
                     sr =  max(aux_u2(inode,2) + sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)), u_buffer(inode,2)+nscbc_c_inf)
                     q_hll   = (sr*aux_q2(inode,2)-sl*nscbc_rho_inf*u_buffer(inode,2)+nscbc_rho_inf*u_buffer(inode,2)**2-aux_u2(inode,2)*aux_q2(inode,2))/(sr-sl)


                     aux_q(inode,2) = q_hll

                     sl = min(u_buffer(inode,3)-nscbc_c_inf, aux_u2(inode,3) - sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)))
                     sr =  max(aux_u2(inode,3) + sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)), u_buffer(inode,3)+nscbc_c_inf)
                     q_hll   = (sr*aux_q2(inode,3)-sl*nscbc_rho_inf*u_buffer(inode,3)+nscbc_rho_inf*u_buffer(inode,3)**2-aux_u2(inode,3)*aux_q2(inode,3))/(sr-sl)

                     aux_q(inode,3) = q_hll

                     aux_u(inode,1) = aux_q(inode,1)/aux_rho(inode)
                     aux_u(inode,2) = aux_q(inode,2)/aux_rho(inode)
                     aux_u(inode,3) = aux_q(inode,3)/aux_rho(inode)

                     aux_p(inode) = aux_rho(inode)*(nscbc_gamma_inf-1.0_rp)*((aux_E(inode)/aux_rho(inode))- &
                        0.5_rp*((aux_u(inode,1)*aux_u(inode,1)) + (aux_u(inode,2)*aux_u(inode,2)) +(aux_u(inode,3)*aux_u(inode,3))))
                        
                  else if (bcode == bc_type_internal_intake) then

                     aux_q(inode,1) = u_buffer(inode,1)*nscbc_rho_inf
                     aux_q(inode,2) = u_buffer(inode,2)*nscbc_rho_inf
                     aux_q(inode,3) = u_buffer(inode,3)*nscbc_rho_inf
   
                     aux_u(inode,1) = u_buffer(inode,1)
                     aux_u(inode,2) = u_buffer(inode,2)
                     aux_u(inode,3) = u_buffer(inode,3)
   
                     aux_rho(inode) = nscbc_rho_inf
                     aux_E(inode)   = (nscbc_rho_inf*0.5_rp*nscbc_u_inf**2 + nscbc_p_inf/(nscbc_gamma_inf-1.0_rp))

                  else if (bcode == bc_type_far_field_supersonic) then
                     
                        aux_q(inode,1) = u_buffer(inode,1)*nscbc_rho_inf
                        aux_q(inode,2) = u_buffer(inode,2)*nscbc_rho_inf
                        aux_q(inode,3) = u_buffer(inode,3)*nscbc_rho_inf
   
                        aux_u(inode,1) = u_buffer(inode,1)
                        aux_u(inode,2) = u_buffer(inode,2)
                        aux_u(inode,3) = u_buffer(inode,3)
   
                        aux_rho(inode) = nscbc_rho_inf
                        aux_E(inode)   = (nscbc_rho_inf*0.5_rp*nscbc_u_inf**2 + nscbc_p_inf/(nscbc_gamma_inf-1.0_rp))
                  else if (bcode == bc_type_recirculation_inlet) then ! recirculation inlet
                     
                     aux_q(inode,1) = (aux_q(lnbn_nodes(inode),1) - u_mapped(inode,1)*aux_rho(lnbn_nodes(inode)))*nscbc_sign_ux
                     aux_q(inode,2) = (aux_q(lnbn_nodes(inode),2) - u_mapped(inode,2)*aux_rho(lnbn_nodes(inode)))*nscbc_sign_uy
                     aux_q(inode,3) = (aux_q(lnbn_nodes(inode),3) - u_mapped(inode,3)*aux_rho(lnbn_nodes(inode)))*nscbc_sign_uz

                     aux_u(inode,1) = (aux_u(lnbn_nodes(inode),1) - u_mapped(inode,1))*nscbc_sign_ux
                     aux_u(inode,2) = (aux_u(lnbn_nodes(inode),2) - u_mapped(inode,2))*nscbc_sign_uy
                     aux_u(inode,3) = (aux_u(lnbn_nodes(inode),3) - u_mapped(inode,3))*nscbc_sign_uz
                     
                     aux_rho(inode) = aux_rho(lnbn_nodes(inode))
                     aux_p(inode)   = aux_p(lnbn_nodes(inode))
                     aux_E(inode)   = aux_rho(inode)*0.5_rp*dot_product(aux_u(inode,:),aux_u(inode,:)) + aux_p(inode)/(aux_rho(inode)*(nscbc_gamma_inf-1.0_rp))
         
#if 0
                     ! riemann fluxes
                     aux_u_mag = dot_product(aux_u(inode,:),aux_u(inode,:))
                     aux_u_2_mag = dot_product(aux_u2(inode,:),aux_u2(inode,:))
                     sl = min(aux_u_mag-sqrt(nscbc_gamma_inf*aux_p(inode)/aux_rho(inode)), aux_u_2_mag - sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)))
                     sr =  max(aux_u_2_mag + sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)), aux_u_mag+sqrt(nscbc_gamma_inf*aux_p(inode)/aux_rho(inode)))

                     rho_hll = (sr*aux_rho2(inode)-sl*aux_rho(inode)+aux_u_mag*aux_rho(inode)-aux_u_2_mag*aux_rho2(inode))/(sr-sl)
                     E_hll   = (sr*aux_E2(inode)-sl*aux_E(inode)+aux_u_mag*(aux_E(inode)+aux_p(inode))-aux_u_2_mag*(aux_E2(inode)+aux_p2(inode)))/(sr-sl)

                     sl = min(aux_u(inode,1)-sqrt(nscbc_gamma_inf*aux_p(inode)/aux_rho(inode)), aux_u2(inode,1) - sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)))
                     sr =  max(aux_u2(inode,1) + sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)), aux_u(inode,1)+sqrt(nscbc_gamma_inf*aux_p(inode)/aux_rho(inode)))

                     q_hll   = (sr*aux_q2(inode,1)-sl*aux_q(inode,1)+aux_q(inode,1)**2-aux_u2(inode,1)*aux_q2(inode,1))/(sr-sl)

                     aux_q(inode,1) = q_hll

                     sl = min(aux_u(inode,2)-sqrt(nscbc_gamma_inf*aux_p(inode)/aux_rho(inode)), aux_u2(inode,2) - sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)))
                     sr =  max(aux_u2(inode,2) + sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)), aux_u(inode,2)+sqrt(nscbc_gamma_inf*aux_p(inode)/aux_rho(inode)))
                     q_hll   = (sr*aux_q2(inode,2)-sl*aux_q(inode,2)+aux_q(inode,2)**2-aux_u2(inode,2)*aux_q2(inode,2))/(sr-sl)


                     aux_q(inode,2) = q_hll

                     sl = min(aux_u(inode,3)-sqrt(nscbc_gamma_inf*aux_p(inode)/aux_rho(inode)), aux_u2(inode,3) - sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)))
                     sr =  max(aux_u2(inode,3) + sqrt(nscbc_gamma_inf*aux_p2(inode)/aux_rho2(inode)), aux_u(inode,3)+sqrt(nscbc_gamma_inf*aux_p(inode)/aux_rho(inode)))
                     q_hll   = (sr*aux_q2(inode,3)-sl*aux_q(inode,3)+aux_q(inode,3)**2-aux_u2(inode,3)*aux_q2(inode,3))/(sr-sl)

                     aux_q(inode,3) = q_hll

                     aux_rho(inode) = rho_hll
                     aux_E(inode) = E_hll
#endif                     

                  else if (bcode == bc_type_non_slip_unsteady) then
                     
                     aux_q(inode,1) = u_buffer(inode,1)*nscbc_rho_inf
                     aux_q(inode,2) = u_buffer(inode,2)*nscbc_rho_inf
                     aux_q(inode,3) = u_buffer(inode,3)*nscbc_rho_inf

                     aux_u(inode,1) = u_buffer(inode,1)
                     aux_u(inode,2) = u_buffer(inode,2)
                     aux_u(inode,3) = u_buffer(inode,3)

                     aux_rho(inode) = nscbc_rho_inf
                     !aux_E(inode) = nscbc_p_inf/(nscbc_gamma_inf-1.0_rp)

                  else if (bcode == bc_type_non_slip_adiabatic) then ! non_slip wall adiabatic

                     aux_q(inode,1) = 0.0_rp
                     aux_q(inode,2) = 0.0_rp
                     aux_q(inode,3) = 0.0_rp

                     aux_u(inode,1) = 0.0_rp
                     aux_u(inode,2) = 0.0_rp
                     aux_u(inode,3) = 0.0_rp

                     !aux_rho(inode) = nscbc_rho_inf
                     !aux_E(inode) = nscbc_p_inf/(nscbc_gamma_inf-1.0_rp)
                     
                  else if (bcode == bc_type_non_slip_hot) then ! non_slip wall hot

                     aux_q(inode,1) = 0.0_rp
                     aux_q(inode,2) = 0.0_rp
                     aux_q(inode,3) = 0.0_rp

                     aux_u(inode,1) = 0.0_rp
                     aux_u(inode,2) = 0.0_rp
                     aux_u(inode,3) = 0.0_rp

                     aux_rho(inode) = nscbc_p_inf/nscbc_Rgas_inf/nscbc_T_H
                     !aux_p(inode) = nscbc_p_inf
                     aux_E(inode) = nscbc_p_inf/(nscbc_gamma_inf-1.0_rp)

                  else if (bcode == bc_type_non_slip_cold) then ! non_slip wall cold

                     aux_q(inode,1) = 0.0_rp
                     aux_q(inode,2) = 0.0_rp
                     aux_q(inode,3) = 0.0_rp

                     aux_u(inode,1) = 0.0_rp
                     aux_u(inode,2) = 0.0_rp
                     aux_u(inode,3) = 0.0_rp

                     aux_rho(inode) = nscbc_p_inf/nscbc_Rgas_inf/nscbc_T_C
                     !aux_p(inode) = nscbc_p_inf
                     aux_E(inode) = nscbc_p_inf/(nscbc_gamma_inf-1.0_rp)

                  else if ((bcode == bc_type_symmetry) ) then 
                     norm = (normalsAtNodes(inode,1)*aux_q(inode,1)) + (normalsAtNodes(inode,2)*aux_q(inode,2)) + (normalsAtNodes(inode,3)*aux_q(inode,3))
                     !$acc loop seq
                     do idime = 1,ndime     
                        aux_q(inode,idime) = aux_q(inode,idime) - norm*normalsAtNodes(inode,idime)
                     end do

                     aux_u(inode,1) = aux_q(inode,1)/aux_rho(inode)
                     aux_u(inode,2) = aux_q(inode,2)/aux_rho(inode)
                     aux_u(inode,3) = aux_q(inode,3)/aux_rho(inode)              
                  else if (bcode == bc_type_slip_isothermal) then ! slip
                     norm = (normalsAtNodes(inode,1)*aux_q(inode,1)) + (normalsAtNodes(inode,2)*aux_q(inode,2)) + (normalsAtNodes(inode,3)*aux_q(inode,3))
                     !$acc loop seq
                     do idime = 1,ndime
                        aux_q(inode,idime) = aux_q(inode,idime) - norm*normalsAtNodes(inode,idime)
                     end do
                     aux_rho(inode) = nscbc_rho_inf

                     aux_u(inode,1) = aux_q(inode,1)/aux_rho(inode)
                     aux_u(inode,2) = aux_q(inode,2)/aux_rho(inode)
                     aux_u(inode,3) = aux_q(inode,3)/aux_rho(inode)

                     aux_E(inode) =  nscbc_p_inf/(nscbc_gamma_inf-1.0_rp) + &
                                    aux_rho(inode)*0.5_rp*((aux_u(inode,1)*aux_u(inode,1)) + (aux_u(inode,2)*aux_u(inode,2)) +(aux_u(inode,3)*aux_u(inode,3)))                  
                  else if ((bcode == bc_type_slip_wall_model) .or. (bcode == bc_type_slip_adiabatic)) then ! slip
                     norm = (normalsAtNodes(inode,1)*aux_q(inode,1)) + (normalsAtNodes(inode,2)*aux_q(inode,2)) + (normalsAtNodes(inode,3)*aux_q(inode,3))
                    ! aux_E(inode) = aux_E(inode) - &
                    ! aux_rho(inode)*0.5_rp*((aux_u(inode,1)*aux_u(inode,1)) + (aux_u(inode,2)*aux_u(inode,2)) +(aux_u(inode,3)*aux_u(inode,3)))
                     !$acc loop seq
                     do idime = 1,ndime     
                        aux_q(inode,idime) = aux_q(inode,idime) - norm*normalsAtNodes(inode,idime)
                     end do
                     !aux_rho(inode) = nscbc_rho_inf

                     aux_u(inode,1) = aux_q(inode,1)/aux_rho(inode)
                     aux_u(inode,2) = aux_q(inode,2)/aux_rho(inode)
                     aux_u(inode,3) = aux_q(inode,3)/aux_rho(inode)

                     !aux_E(inode) =  nscbc_p_inf/(nscbc_gamma_inf-1.0_rp) + &
                     !               aux_rho(inode)*0.5_rp*((aux_u(inode,1)*aux_u(inode,1)) + (aux_u(inode,2)*aux_u(inode,2)) +(aux_u(inode,3)*aux_u(inode,3)))
                     !aux_E(inode) = aux_E(inode) + &
                     !aux_rho(inode)*0.5_rp*((aux_u(inode,1)*aux_u(inode,1)) + (aux_u(inode,2)*aux_u(inode,2)) +(aux_u(inode,3)*aux_u(inode,3)))
                  else if ((bcode == bc_type_slip_wall_model_iso)) then ! slip
                     norm = (normalsAtNodes(inode,1)*aux_q(inode,1)) + (normalsAtNodes(inode,2)*aux_q(inode,2)) + (normalsAtNodes(inode,3)*aux_q(inode,3))       
                     aux_E(inode) = aux_E(inode) - &
                     aux_rho(inode)*0.5_rp*((aux_u(inode,1)*aux_u(inode,1)) + (aux_u(inode,2)*aux_u(inode,2)) +(aux_u(inode,3)*aux_u(inode,3)))      
                     !$acc loop seq
                     do idime = 1,ndime     
                        aux_q(inode,idime) = aux_q(inode,idime) - norm*normalsAtNodes(inode,idime)
                     end do
                     aux_rho(inode) = nscbc_rho_inf

                     aux_u(inode,1) = aux_q(inode,1)/aux_rho(inode)
                     aux_u(inode,2) = aux_q(inode,2)/aux_rho(inode)
                     aux_u(inode,3) = aux_q(inode,3)/aux_rho(inode)
                     
                     aux_E(inode) =  nscbc_p_inf/(nscbc_gamma_inf-1.0_rp) + &
                                    aux_rho(inode)*0.5_rp*((aux_u(inode,1)*aux_u(inode,1)) + (aux_u(inode,2)*aux_u(inode,2)) +(aux_u(inode,3)*aux_u(inode,3)))

                  else if ((bcode == bc_type_slip_atmosphere)) then ! slip
                     ! Compute value for the density
                     y = coordPar(inode,2)
                     ! Set up the atmosphere, Navas-Montilla (2023) eq. 71 (adiabatic atmosphere)
                     ! https://farside.ph.utexas.edu/teaching/sm1/lectures/node56.html
                     rr    = (nscbc_gamma_inf - 1.0_rp)/nscbc_gamma_inf*nscbc_g/nscbc_Rgas_inf/nscbc_T_C ! Reused variable
                     T_inf = nscbc_T_C*(1.0_rp - rr*y)
                     p_inf = nscbc_p_inf*(1.0_rp - rr*y)**(nscbc_Cp_inf/nscbc_Rgas_inf)
                     ! WARNING: here for numerical approximation is important to either use
                     ! (gamma-1)/gamma or Cp/R as otherwise we incur in approximation issues!
                     
                     ! Density is set with the equation of state
                     aux_rho(inode) = p_inf/nscbc_Rgas_inf/T_inf
                     
                     aux_E(inode) = aux_E(inode) - &
                                    aux_rho(inode)*0.5_rp*((aux_u(inode,1)*aux_u(inode,1)) + (aux_u(inode,2)*aux_u(inode,2)) +(aux_u(inode,3)*aux_u(inode,3)))
                     
                     norm = (normalsAtNodes(inode,1)*aux_q(inode,1)) + (normalsAtNodes(inode,2)*aux_q(inode,2)) + (normalsAtNodes(inode,3)*aux_q(inode,3))
                     !$acc loop seq
                     do idime = 1,ndime     
                        aux_q(inode,idime) = aux_q(inode,idime) - norm*normalsAtNodes(inode,idime)
                     end do

                     aux_u(inode,1) = aux_q(inode,1)/aux_rho(inode)
                     aux_u(inode,2) = aux_q(inode,2)/aux_rho(inode)
                     aux_u(inode,3) = aux_q(inode,3)/aux_rho(inode)

                     aux_E(inode) = aux_E(inode) + &
                                    aux_rho(inode)*0.5_rp*((aux_u(inode,1)*aux_u(inode,1)) + (aux_u(inode,2)*aux_u(inode,2)) +(aux_u(inode,3)*aux_u(inode,3)))
                               
                  end if
               end if
            end do
            !$acc end parallel loop

         end subroutine temporary_bc_routine_dirichlet_prim
subroutine bc_fix_dirichlet_residual(npoin,nboun,bou_codes,bou_codes_nodes,bound,nbnodes,lbnodes,lnbn_nodes,normalsAtNodes,Rmass,Rmom,Rener)

            implicit none

            integer(4), intent(in)     :: npoin, nboun, bou_codes(nboun), bou_codes_nodes(npoin), bound(nboun,npbou)
            integer(4), intent(in)     :: nbnodes, lbnodes(nbnodes),lnbn_nodes(npoin)
            real(rp), intent(in)     :: normalsAtNodes(npoin,ndime)
            real(rp),    intent(inout) :: Rmass(npoin),Rmom(npoin,ndime),Rener(npoin)
            integer(4)                 :: iboun,bcode,ipbou,inode,idime,iBoundNode


            !$acc parallel loop
            do inode = 1,npoin
               if(bou_codes_nodes(inode) .lt. max_num_bou_codes) then
                  bcode = bou_codes_nodes(inode) ! Boundary element code
                  if (bcode == bc_type_non_slip_adiabatic) then ! non_slip wall adiabatic
                     Rmom(inode,1) = 0.0_rp
                     Rmom(inode,2) = 0.0_rp
                     Rmom(inode,3) = 0.0_rp

                     Rmass(inode) = 0.0_rp
                  else if (bcode == bc_type_non_slip_unsteady) then ! non_slip wall adiabatic
                     Rmom(inode,1) = 0.0_rp
                     Rmom(inode,2) = 0.0_rp
                     Rmom(inode,3) = 0.0_rp

                     Rmass(inode) = 0.0_rp
                  else if (bcode == bc_type_non_slip_hot) then ! non_slip wall hot
                     Rmom(inode,1) = 0.0_rp
                     Rmom(inode,2) = 0.0_rp
                     Rmom(inode,3) = 0.0_rp

                     Rmass(inode) = 0.0_rp
                     Rener(inode) = 0.0_rp
                  else if (bcode == bc_type_non_slip_cold) then ! non_slip wall cold
                     Rmom(inode,1) = 0.0_rp
                     Rmom(inode,2) = 0.0_rp
                     Rmom(inode,3) = 0.0_rp

                     Rmass(inode) = 0.0_rp
                     Rener(inode) = 0.0_rp
                  else if (bcode == bc_type_far_field_supersonic) then ! non_slip wall cold
                     Rmom(inode,1) = 0.0_rp
                     Rmom(inode,2) = 0.0_rp
                     Rmom(inode,3) = 0.0_rp

                     Rmass(inode) = 0.0_rp
                     Rener(inode) = 0.00_rp
                  else if (bcode == bc_type_recirculation_inlet) then ! non_slip wall cold
                     Rmom(inode,1) = 0.0_rp
                     Rmom(inode,2) = 0.0_rp
                     Rmom(inode,3) = 0.0_rp

                     Rmass(inode) = 0.0_rp
                     Rener(inode) = 0.0_rp
                  end if
               end if
            end do
            !$acc end parallel loop

         end subroutine bc_fix_dirichlet_residual

         subroutine bc_fix_dirichlet_Jacobian(npoin,nboun,bou_codes,bou_codes_nodes,bound,nbnodes,lbnodes,lnbn_nodes,normalsAtNodes,Jmass,Jmom,Jener)

            implicit none

            integer(4), intent(in)     :: npoin, nboun, bou_codes(nboun), bou_codes_nodes(npoin), bound(nboun,npbou)
            integer(4), intent(in)     :: nbnodes, lbnodes(nbnodes),lnbn_nodes(npoin)
            real(rp), intent(in)     :: normalsAtNodes(npoin,ndime)
            real(rp),    intent(inout) :: Jmass(npoin),Jmom(npoin,ndime),Jener(npoin)
            integer(4)                 :: iboun,bcode,ipbou,inode,idime,iBoundNode


            !$acc parallel loop
            do inode = 1,npoin
               if(bou_codes_nodes(inode) .lt. max_num_bou_codes) then
                  bcode = bou_codes_nodes(inode) ! Boundary element code
                  if (bcode == bc_type_non_slip_adiabatic) then ! non_slip wall adiabatic
                     Jmom(inode,1) = 0.0_rp
                     Jmom(inode,2) = 0.0_rp
                     Jmom(inode,3) = 0.0_rp
                     Jmass(inode) = 0.0_rp

                  else if (bcode == bc_type_non_slip_hot) then ! non_slip wall hot
                     Jmom(inode,1) = 0.0_rp
                     Jmom(inode,2) = 0.0_rp
                     Jmom(inode,3) = 0.0_rp

                     Jmass(inode) = 0.0_rp
                     Jener(inode) = 0.0_rp
                  else if (bcode == bc_type_non_slip_cold) then ! non_slip wall cold
                     Jmom(inode,1) = 0.0_rp
                     Jmom(inode,2) = 0.0_rp
                     Jmom(inode,3) = 0.0_rp

                     Jmass(inode) = 0.0_rp
                     Jener(inode) = 0.0_rp
                  end if
               end if
            end do
            !$acc end parallel loop

         end subroutine bc_fix_dirichlet_Jacobian

         subroutine bc_fix_dirichlet_residual_entropy(npoin,nboun,bou_codes,bou_codes_nodes,bound,nbnodes,lbnodes,lnbn_nodes,normalsAtNodes,R)

            implicit none

            integer(4), intent(in)     :: npoin, nboun, bou_codes(nboun), bou_codes_nodes(npoin), bound(nboun,npbou)
            integer(4), intent(in)     :: nbnodes, lbnodes(nbnodes),lnbn_nodes(npoin)
            real(rp), intent(in)     :: normalsAtNodes(npoin,ndime)
            real(rp),    intent(inout) :: R(npoin)
            integer(4)                 :: iboun,bcode,ipbou,inode,idime,iBoundNode


            !$acc parallel loop
            do inode = 1,npoin
               if(bou_codes_nodes(inode) .lt. max_num_bou_codes) then
                  bcode = bou_codes_nodes(inode) ! Boundary element code
                  if (bcode == bc_type_non_slip_adiabatic) then ! non_slip wall adiabatic
                     R(inode) = 0.0_rp
                  else if (bcode == bc_type_non_slip_hot) then ! non_slip wall hot
                     R(inode) = 0.0_rp
                  else if (bcode == bc_type_non_slip_cold) then ! non_slip wall cold
                     R(inode) = 0.0_rp
                  end if
               end if
            end do
            !$acc end parallel loop

         end subroutine bc_fix_dirichlet_residual_entropy

         subroutine copy_periodicNodes_for_mappedInlet(q,u,rho,E,p)
            implicit none
            real(rp),intent(inout) :: rho(numNodesRankPar),q(numNodesRankPar,ndime),u(numNodesRankPar,ndime),p(numNodesRankPar),E(numNodesRankPar)
            integer(4) :: iPer

            !$acc parallel loop
            do iPer = 1,nPerRankPar
               u(masSlaRankPar(iPer,2),1) = u(masSlaRankPar(iPer,1),1)
               u(masSlaRankPar(iPer,2),2) = u(masSlaRankPar(iPer,1),2)
               u(masSlaRankPar(iPer,2),3) = u(masSlaRankPar(iPer,1),3)

               q(masSlaRankPar(iPer,2),1) = q(masSlaRankPar(iPer,1),1)
               q(masSlaRankPar(iPer,2),2) = q(masSlaRankPar(iPer,1),2)
               q(masSlaRankPar(iPer,2),3) = q(masSlaRankPar(iPer,1),3)
               
               rho(masSlaRankPar(iPer,2)) = rho(masSlaRankPar(iPer,1))
               E(masSlaRankPar(iPer,2))   = E(masSlaRankPar(iPer,1))
               p(masSlaRankPar(iPer,2))   = p(masSlaRankPar(iPer,1))
            end do
            !$acc end parallel loop

         end subroutine copy_periodicNodes_for_mappedInlet


         subroutine temporary_bc_routine_dirichlet_prim_solver(npoin,nboun,bou_codes,bou_codes_nodes,bound,nbnodes,lbnodes,lnbn_nodes,normalsAtNodes,aux_rho_r,aux_q,aux_E,u_buffer)

            implicit none

            integer(4), intent(in)     :: npoin, nboun, bou_codes(nboun), bou_codes_nodes(npoin), bound(nboun,npbou)
            integer(4), intent(in)     :: nbnodes, lbnodes(nbnodes),lnbn_nodes(npoin)
            real(rp), intent(in)     :: normalsAtNodes(npoin,ndime),u_buffer(npoin,ndime)
            real(rp),    intent(inout) :: aux_rho_r(npoin),aux_q(npoin,ndime),aux_E(npoin)
            integer(4)                 :: iboun,bcode,ipbou,inode,idime,iBoundNode
            real(rp)                   :: cin,R_plus,R_minus,v_b,c_b,s_b,rho_b,p_b,rl,rr, sl, sr
            real(rp)                   :: q_hll,rho_hll,E_hll,E_inf,norm

         

            !$acc parallel loop  
            do inode = 1,npoin
               if(bou_codes_nodes(inode) .lt. max_num_bou_codes) then
                  bcode = bou_codes_nodes(inode) ! Boundary element code
                   if (bcode == bc_type_non_slip_adiabatic) then ! non_slip wall adiabatic
                     
                     aux_q(inode,1) = 0.0_rp
                     aux_q(inode,2) = 0.0_rp
                     aux_q(inode,3) = 0.0_rp

                     aux_rho_r(inode) = 0.0_rp
                     
                  else if ((bcode == bc_type_slip_wall_model) .or. (bcode == bc_type_slip_adiabatic) .or. (bcode == bc_type_slip_atmosphere)) then ! slip)
                     norm = (normalsAtNodes(inode,1)*aux_q(inode,1)) + (normalsAtNodes(inode,2)*aux_q(inode,2)) + (normalsAtNodes(inode,3)*aux_q(inode,3))
                     !$acc loop seq
                     do idime = 1,ndime     
                        aux_q(inode,idime) = aux_q(inode,idime) - norm*normalsAtNodes(inode,idime)
                     end do
                     aux_rho_r(inode) = 0.0_rp
                  end if
               end if
            end do
            !$acc end parallel loop

         end subroutine temporary_bc_routine_dirichlet_prim_solver

      end module mod_bc_routines
