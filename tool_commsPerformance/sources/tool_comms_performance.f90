
program tool_commsPerfomance
    use mod_mpi
    use mod_read_inputFile
    use mod_mpi_mesh
    use mod_comms
    use mod_hdf5
    use mod_comms_performance
    implicit none

    character(999) :: input_file
    integer :: lineCnt
    integer :: numNodesSrl,numNodesB_1r,numIters
    character(256) :: parameter2read
    character(512) :: mesh_h5_file_path,mesh_h5_file_name,meshFile_h5_full_name
    character(1024) :: log_file_comms,log_file_append
    character(128) :: aux_string_mpisize,aux_string_numNodes,aux_string_nodesBound

    logical :: useMesh=.false.

    call init_mpi()

    !------------------------------------------------------------------------------
    ! Reading input file
    if(command_argument_count() .eq. 1) then
        call get_command_argument(1, input_file)
        if(mpi_rank.eq.0) write(*,*) '# Input file: ',trim(adjustl(input_file))
    else
        if(mpi_rank.eq.0) write(*,*) 'You must call Tool COMMS PERFROMANCE with an input file!'
        call MPI_Abort(app_comm,-1,mpi_err)
    endif
    !------------------------------------------------------------------------------
    ! Reading the parameters
    call open_inputFile(input_file)
    lineCnt = 1

    !1. numIters--------------------------------------------------------------------------
    parameter2read = 'numIters'
    call read_inputFile_integer(lineCnt,parameter2read,numIters)

    !2. Use mesh--------------------------------------------------------------------------
    parameter2read = 'useMesh'
    call read_inputFile_logical(lineCnt,parameter2read,useMesh)

    if(useMesh) then
        !3. mesh_h5_file_path--------------------------------------------------------------
        parameter2read = 'mesh_h5_file_path'
        call read_inputFile_string(lineCnt,parameter2read,mesh_h5_file_path)

        !4. mesh_h5_file_name--------------------------------------------------------------
        parameter2read = 'mesh_h5_file_name'
        call read_inputFile_string(lineCnt,parameter2read,mesh_h5_file_name)
    else
        !3. numNodesSrl--------------------------------------------------------------------------
        parameter2read = 'numNodesSrl'
        call read_inputFile_integer(lineCnt,parameter2read,numNodesSrl)

        !4. numNodesB_1r --------------------------------------------------------------------------
        parameter2read = 'numNodesB_1r'
        call read_inputFile_integer(lineCnt,parameter2read,numNodesB_1r)

       if(mpi_rank.eq.0) write(*,*) 'numNodesSrl ',numNodesSrl,' numNodesB_1r ',numNodesB_1r,' numIters ',numIters
    end if

    call close_inputFile()

    !-----------------------------------------------------------------------------------
    if(useMesh) then
        call init_hdf5_interface()
        call set_hdf5_meshFile_name(mesh_h5_file_path,mesh_h5_file_name,mpi_size,meshFile_h5_full_name)
        call load_hdf5_meshfile(meshFile_h5_full_name)
    else
        call create_dummy_1Dmesh(numNodesSrl,numNodesB_1r)
    end if

    if(mpi_rank.eq.0) then
        write(aux_string_mpisize,'(I0)') mpi_size
        if(useMesh) then
            log_file_append = trim(adjustl(mesh_h5_file_name))
        else
            write(aux_string_numNodes,'(I0)') numNodesSrl
            write(aux_string_nodesBound,'(I0)') numNodesB_1r
            log_file_append = 'mesh1D_'//trim(aux_string_numNodes)//'_bound_'//trim(aux_string_nodesBound)
        end if
        log_file_comms = 'commPerf_'//trim(adjustl(log_file_append))//'-'//trim(aux_string_mpisize)//'.dat'
    end if

   call test_comms_performance_real(numIters,log_file_comms)

   call end_comms()
   call end_mpi()

end program tool_commsPerfomance
